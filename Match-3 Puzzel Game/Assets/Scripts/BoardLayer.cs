﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardLayer : MonoBehaviour
{
    public LayoutRow[] allRows;

    public Gem[,] GetLayout()
    {
        Gem[,] theLayout = new Gem[allRows[0].gemInRow.Length, allRows.Length];

        for (int y = 0; y < allRows.Length; y++)
        {
            for (int x = 0; x < allRows[y].gemInRow.Length; x++)
            {
                if(x < theLayout.GetLength(0))
                {
                    if (allRows[y].gemInRow[x] != null)
                    {
                        theLayout[x, allRows.Length - 1 - y] = allRows[y].gemInRow[x];
                    }
                }
            }
        }


        return theLayout;
    }
}


[System.Serializable]
public class LayoutRow
{
    public Gem[] gemInRow;
}
