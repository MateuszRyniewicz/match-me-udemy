﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asdd : MonoBehaviour
{

    
   
    public int wight;
    public int height;

    public GameObject bgTitlePrefab;

    public Gem[] gems;

  
    void Start()
    {
        Setup();
    }

    public void Setup()
    {
        for (int x = 0; x < wight; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector2 pos = new Vector2(x, y);
                GameObject bgTitle = Instantiate(bgTitlePrefab, pos, Quaternion.identity);
                bgTitle.transform.parent = transform;
                bgTitle.name = "BG Tile -" + x + "," + y;

                int gemToUse = Random.Range(0, gems.Length);
                SpawnGem(new Vector2Int(x, y), gems[gemToUse]);

            }
        }
    }

    public void SpawnGem(Vector2Int pos, Gem gemToSpawn)
    {
        Gem gem = Instantiate(gemToSpawn, (Vector2)pos, Quaternion.identity);
        gem.transform.parent = this.transform;
        gem.name = "Gem" + pos.x + ", " + pos.y;
    }
}

